# Lab5 -- Integration testing


## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Tests

The table of tests can be found:

* By [link](https://docs.google.com/spreadsheets/d/1fJuC8x2Nu9hbwOTFmiW5u21wvUDqDMAHRqfyPIqV_bg/edit?pli=1#gid=0)

* In file `tests.xlsx`